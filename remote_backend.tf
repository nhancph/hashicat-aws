terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "CK-Demo-Organization"
    workspaces {
      name = "hashicat-aws"
    }
  }
}
